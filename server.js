const express = require('express'),
    app = express(),
    mongoose = require('mongoose'),
    config = require('./config'),
    bodyParser = require('body-parser'),
    http = require('http');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json())

mongoose.connect(config.database, {
    useNewUrlParser: true, useUnifiedTopology: true
})


require('./src/routes')(app)

const port = config.port
app.set('port', port)

const server = http.createServer(app)
server.listen(port)
console.log('Your server is running on port ' + port)

