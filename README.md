## Install with docker

```docker-compose build```
```docker-compose up -d```


### Connect to mongo server then:
```mongo > config = {_id:"rs0", members:[ {_id:0, host:"127.0.0.1:27017"}]}```

```mongo > rs.initiate(config)```

## Tests
```docker-compose run mocha src/tests/users.js ```
