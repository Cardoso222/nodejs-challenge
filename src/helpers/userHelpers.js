const userModel = require('../models/user');

module.exports = {
    validateUserParams(params) {
        let { username } = params.user;
        let { curriculum } = params;
        return (username && curriculum)

    },

    async isDuplicatedUser(param) {
        let { username } = param.user;
        let result = await userModel.countDocuments({ username: username });
        return !!result;
    }
}