const userModel = require('../models/user'),
    CurriculumModel = require('../models/curriculum'),
    mongoose = require('mongoose'),
    { validateUserParams, isDuplicatedUser } = require('../helpers/userHelpers')

module.exports = {
    async create(req, res) {
        const session = await mongoose.startSession()
        try {
            session.startTransaction()

            if (!validateUserParams(req.body)) {
                throw new Error('Error: Invalid user params');
            }
            if (await isDuplicatedUser(req.body)) {
                throw new Error('Username already exist');
            }

            let { user, curriculum } = req.body;
            const _curriculum = new CurriculumModel({
                ...curriculum
            })

            let curriculumInfo = await _curriculum.save({ session });

            const _user = new userModel({
                ...user,
                curriculum: curriculumInfo._id
            });

            await _user.save({ session });
            await session.commitTransaction();
            return res.status(200).json({ message: 'User Successfully created!' });
        }
        catch (err) {
            console.log(err);
            await session.abortTransaction()
            return res.status(500).json({ error: err.message });
        } finally {
            session.endSession();
        }

    },

    async showAll(req, res) {
        try {
            let response = await userModel.find().populate('curriculum');
            return res.status(200).json({ users: response });
        } catch (err) {
            console.log(err);
            return res.status(500).json({ error: err.message });
        }

    }
}