const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
    name: {
        type: String
    },
    address: {
        type: String
    },
    username: {
        type: String
    },
    curriculum: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Curriculum'
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('User', UserSchema)
