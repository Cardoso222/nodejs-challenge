const mongoose = require('mongoose')

const CurriculumSchema = new mongoose.Schema({
    type: {
        type: String
    },
    professional_experiences: [{
        type: String
    }],
    qualifications: [{
        type: String
    }],
    languages: [{
        type: String
    }]
}, {
    timestamps: true
});

module.exports = mongoose.model('Curriculum', CurriculumSchema)
