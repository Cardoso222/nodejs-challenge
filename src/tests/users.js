const request = require('request'),
    UserModel = require('../models/user'),
    Curriculum = require('../models/curriculum'),
    mongoose = require('mongoose'),
    config = require('../../config'),
    assert = require('assert'),
    userInfo = require('./userInfo.json'),
    invalidUserInfo = require('./invalidUserInfo.json'),
    url = 'http://app:3000';

mongoose.connect(config.database, {
    useNewUrlParser: true, useUnifiedTopology: true
})

describe('Users', function () {
    describe('Create a new user', function () {
        it('should return status code 200', function (done) {
            request.post({
                url: url + '/api/users',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(userInfo)
            }, function (error, res, body) {
                if (error) {
                    console.log(error);
                }

                assert.equal(JSON.parse(body).message, 'User Successfully created!');
                assert.equal(res.statusCode, 200);
                done();
            })
        });

        it('invalid json should return error message', function (done) {
            request.post({
                url: url + '/api/users',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(invalidUserInfo)
            }, function (error, res, body) {
                if (error) {
                    console.log(error);
                }

                assert.equal(JSON.parse(body).error, "Error: Invalid user params");
                assert.equal(res.statusCode, 500);
                done();
            })
        });
    });
    describe('show all users', function () {
        it('should return status code 200', function (done) {
            request.get({
                url: url + '/api/users/showAll'
            }, function (error, res, body) {
                if (error) {
                    console.log(error);
                }

                let response = JSON.parse(body);
                assert.equal(response.users[0].hasOwnProperty('name'), true);
                assert.equal(response.users[0].hasOwnProperty('username'), true);
                assert.equal(response.users[0].hasOwnProperty('address'), true);
                assert.equal(response.users[0].hasOwnProperty('curriculum'), true);
                assert.equal(response.users[0].curriculum.hasOwnProperty('professional_experiences'), true);
                assert.equal(response.users[0].curriculum.hasOwnProperty('qualifications'), true);
                assert.equal(response.users[0].curriculum.hasOwnProperty('languages'), true);

                assert.equal(res.statusCode, 200);
                done();
            })
        });
    })
    after(async function () {
        let _user = await UserModel.findOne({ username: userInfo.user.username });
        if (_user) {
            await Curriculum.deleteOne({ _id: _user.curriculum });
            await UserModel.deleteOne({ username: userInfo.user.username });
        }

        mongoose.connection.close()
    })

});