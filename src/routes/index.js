const apiRoutes = require('express').Router(),
    userController = require('../controllers/user')

module.exports = function (app) {
    apiRoutes.post('/users', userController.create);
    apiRoutes.get('/users/showAll', userController.showAll);


    app.use('/api', apiRoutes);
};
